<?php

namespace App\Services;

use Stripe\StripeClient;

class StripePayment
{
    protected $stripe;

    public function __construct()
    {
        $this->stripe = new StripeClient(config('services.stripe.secret'));
    }

    public function createDirectDebitPayment($email, $name, $amount)
    {
        // Create new customer
        $customer = $this->stripe->customers->create([
            'email' => $email,
            'name' => $name,
            'payment_method' => 'pm_abc123',
            'invoice_settings' => [
                'default_payment_method' => 'pm_abc123',
            ],
        ]);

        // Create mandate to authorise future payments
        $mandate = $this->stripe->mandates->create([
            'customer' => $customer->id,
            'payment_method' => 'pm_abc123',
            'type' => 'bacs_debit',
        ]);

        // Create payment intent to charge the customer account
        $paymentIntent = $this->stripe->paymentIntents->create([
            'amount' => $amount,
            'currency' => 'gbp',
            'customer' => $customer->id,
            'payment_method' => $mandate->payment_method,
            'off_session' => true,
            'confirm' => true,
        ]);

        // Check if payment successful
        if ($paymentIntent->status == 'succeeded') {
            return true;
        } else {
            return false;
        }
    }
}
